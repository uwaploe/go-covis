package cmd

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"os"

	"bitbucket.org/uwaploe/go-covis/pkg/platform"
	"bitbucket.org/uwaploe/go-covis/pkg/publish/publog"
	"bitbucket.org/uwaploe/go-covis/pkg/translator"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

// levelCmd represents the level command
var levelCmd = &cobra.Command{
	Use:   "level",
	Short: "Level the COVIS platform",
	RunE: func(cmd *cobra.Command, args []string) error {
		conn, err := grpcClient(viper.GetString("server"))
		if err != nil {
			return fmt.Errorf("could not connect: %v", err)
		}
		client := translator.NewTranslatorClient(conn)
		pub := publog.New(log.New(os.Stderr, "", log.LstdFlags))
		p := platform.New(nil, client, pub)
		state, err := p.Level(context.Background())
		if err == nil {

			b, err := json.Marshal(state)
			if err == nil {
				fmt.Printf("%s\n", b)
			}
		}
		return err
	},
}

func init() {
	rootCmd.AddCommand(levelCmd)
}
