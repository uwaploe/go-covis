package cmd

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"os"

	"bitbucket.org/uwaploe/go-covis/pkg/platform"
	"bitbucket.org/uwaploe/go-covis/pkg/publish/publog"
	"bitbucket.org/uwaploe/go-covis/pkg/translator"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

// moveCmd represents the move command
var moveCmd = &cobra.Command{
	Use:   "move",
	Short: "Change the relative orientation of the platform",
	RunE: func(cmd *cobra.Command, args []string) error {
		conn, err := grpcClient(viper.GetString("server"))
		if err != nil {
			return fmt.Errorf("could not connect: %v", err)
		}
		client := translator.NewTranslatorClient(conn)
		pub := publog.New(log.New(os.Stderr, "", log.LstdFlags))
		p := platform.New(nil, client, pub)
		angle := platform.Angle{
			Roll:    viper.GetFloat64("roll"),
			Pitch:   viper.GetFloat64("pitch"),
			Heading: viper.GetFloat64("heading"),
		}
		state, err := p.Move(context.Background(), &angle)
		if err == nil {
			b, err := json.Marshal(state)
			if err == nil {
				fmt.Printf("%s\n", b)
			}
		}
		return err
	},
}

func init() {
	rootCmd.AddCommand(moveCmd)

	moveCmd.Flags().Float64P("roll", "r", 0, "Change in roll angle (degrees)")
	moveCmd.Flags().Float64P("pitch", "p", 0, "Change in pitch angle (degrees)")
	moveCmd.Flags().Float64("heading", 0, "Change in heading angle (degrees)")
	viper.BindPFlag("roll", moveCmd.Flags().Lookup("roll"))
	viper.BindPFlag("pitch", moveCmd.Flags().Lookup("pitch"))
	viper.BindPFlag("heading", moveCmd.Flags().Lookup("heading"))
}
