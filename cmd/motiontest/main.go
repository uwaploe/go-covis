package main

import "bitbucket.org/uwaploe/go-covis/cmd/motiontest/cmd"

var Version = "dev"
var BuildDate = "unknown"

func main() {
	cmd.Execute(Version)
}
