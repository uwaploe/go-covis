// Sweeprunner provides a service to run COVIS data collection sequences
// (sweeps).  Sweep configurations are read from a queue implemented as a
// Redis list.
package main

import (
	"context"
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"os/signal"
	"runtime"
	"runtime/debug"
	"syscall"
	"time"

	"bitbucket.org/uwaploe/go-covis/pkg/platform"
	"bitbucket.org/uwaploe/go-covis/pkg/publish/pubredis"
	"bitbucket.org/uwaploe/go-covis/pkg/sweep"
	"bitbucket.org/uwaploe/go-covis/pkg/translator"
	reson "bitbucket.org/uwaploe/go-reson"
	"github.com/garyburd/redigo/redis"
	"github.com/pkg/errors"
	"google.golang.org/grpc"
)

var Version = "dev"
var BuildDate = "unknown"

var usage = `
Usage: sweeprunner [options] datadir

Service to collect data from the COVIS sonar.
`

var (
	showvers = flag.Bool("version", false,
		"Show program version information and exit")
	dumpcfg = flag.Bool("dump", false,
		"Dump the default configuration to standard output and exit")
	config  = flag.String("config", "", "Configuration file")
	timeout = flag.Duration("timeout", 3*time.Second,
		"Maximum time between sonar data packets")
	dataPrefix = flag.String("prefix", "COVIS", "Data-set prefix")
)

type ConnectError string

func (e ConnectError) Error() string { return "cannot connect " + string(e) }

type InvalidTransducer string

func (e InvalidTransducer) Error() string { return "invalid transducer " + string(e) }

type OpError struct {
	t   time.Time
	err error
	sw  *sweep.Sweep
}

func NewOpError(err error, sw *sweep.Sweep) OpError {
	return OpError{
		t:   time.Now().UTC(),
		err: err,
		sw:  sw}
}

func (e OpError) Error() string {
	return e.err.Error()
}

func (e OpError) MarshalJSON() ([]byte, error) {
	m := make(map[string]interface{})
	m["t"] = e.t
	m["error"] = e.Error()
	m["sweep"] = e.sw
	return json.Marshal(m)
}

// Time to wait between shutting down the 7kCenter server and requesting a restart.
const serverShutdownTime time.Duration = time.Second * 8

func nextSweep(conn redis.Conn, key, datadir, prefix string) (*sweep.Sweep, error) {
	var sw sweep.Sweep

	log.Println("Waiting for sweep configuration")
	v, err := redis.Values(conn.Do("BRPOP", key, 0))
	if err != nil {
		return nil, err
	}

	err = json.Unmarshal(v[1].([]byte), &sw)
	if err != nil {
		return nil, err
	}

	// Generate a sweep data directory name
	sw.Dir = fmt.Sprintf("%s/%s-%s-%s",
		datadir,
		prefix,
		time.Now().UTC().Format("20060102T150405"),
		sw.Type)

	return &sw, nil
}

// Start Reson 7kCenter server with the proper transducer
func start7kCenter(cfg sonarConfig, xducer string) (reson.Conn, []reson.DevInfo, error) {
	// 7kCenter can be in one of three states
	//    1. down
	//    2. up and using the transducer that we want
	//    3. up and using a different transducer
	//
	conn, err := reson.Dial(cfg.Address, *timeout)
	if err != nil {
		return nil, nil, ConnectError(err.Error())
	}

	devs, err := conn.GetDevices()
	if err != nil {
		conn.Close()
		return nil, nil, errors.Wrap(err, "GetDevices")
	}

	if devs[0].Name != xducer {
		conn.Shutdown()
		conn.Close()
		return nil, nil, InvalidTransducer(devs[0].Name)
	}

	conn.SetEndpoint(devs[0].Deviceid, devs[0].Enumerator)

	return conn, devs, nil
}

// Start a gRPC client to control the Translator subsystem.
func grpcClient(cfg transConfig) (*grpc.ClientConn, error) {
	var opts []grpc.DialOption
	opts = append(opts, grpc.WithInsecure())

	conn, err := grpc.Dial(cfg.Address, opts...)
	if err != nil {
		return nil, err
	}

	return conn, err
}

func main() {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, usage)
		flag.PrintDefaults()
	}

	flag.Parse()
	if *showvers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	if *dumpcfg {
		fmt.Printf("%s", DEFAULT)
		os.Exit(0)
	}

	args := flag.Args()
	if len(args) < 1 {
		fmt.Fprintln(os.Stderr, "Missing data directory argument")
		flag.Usage()
		os.Exit(1)
	}

	var (
		err error
		cfg sysConfig
	)

	if err = os.MkdirAll(args[0], 0755); err != nil {
		log.Fatalf("Make data directory: %v", err)
	}

	if err = loadConfiguration(DEFAULT, *config, &cfg); err != nil {
		log.Fatal(err)
	}

	// Map transducer IDs to RCM command codes and transducer names
	xcodes := make(map[string]uint)
	xnames := make(map[string]string)
	for _, x := range cfg.Sonar.Transducer {
		xcodes[x.Id] = x.Code
		xnames[x.Id] = x.Name
	}
	log.Printf("Transducer names: %v", xnames)
	log.Printf("Transducer codes: %v", xcodes)

	// Redis connection to access the command queue
	qconn, err := redis.Dial("tcp", cfg.Redis.Address)
	if err != nil {
		log.Fatalf("Cannot access Redis: %v", err)
	}
	defer qconn.Close()

	// Open a second connection for the Publisher
	pconn, err := redis.Dial("tcp", cfg.Redis.Address)
	if err != nil {
		log.Fatalf("Cannot access Redis: %v", err)
	}
	pub := pubredis.New(pconn)

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	// Initialize signal handler
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT,
		syscall.SIGTERM, syscall.SIGHUP, syscall.SIGPIPE)
	defer signal.Stop(sigs)

	// Goroutine to wait for signals or for the signal channel
	// to close.
	go func() {
		s, more := <-sigs
		if more {
			log.Printf("Got signal: %v", s)
			cancel()
		}
	}()

mainloop:
	for {
		sw, err := nextSweep(qconn, cfg.Redis.Queue, args[0], *dataPrefix)
		if err != nil {
			log.Printf("nextSweep: %v", err)
			continue
		}

		if err = os.MkdirAll(sw.Dir, 0755); err != nil {
			log.Printf("Make data directory: %v", err)
			continue
		}

		// Open connection to sonar
		log.Printf("Using transducer: %s", sw.Transducer)
		conn, devs, err := start7kCenter(cfg.Sonar, xnames[sw.Transducer])
		if err != nil {
			switch e := err.(type) {
			case ConnectError:
				// 7kCenter not running
				err = reson.RcmRun(cfg.Rcm.Address, xcodes[sw.Transducer])
				if err != nil {
					log.Printf("RcmRun: %v", err)
					pub.Publish("covis.error", NewOpError(err, sw))
					continue mainloop
				}
				log.Println("Allowing 15 seconds for 7kCenter to start")
				if !waitPortOpen(ctx, cfg.Sonar.Address, 15*time.Second) {
					log.Println("7kCenter not starting")
					pub.Publish("covis.error", NewOpError(fmt.Errorf("7kCenter not starting"), sw))
					continue mainloop
				}
				conn, devs, err = start7kCenter(cfg.Sonar, xnames[sw.Transducer])
				if err != nil {
					log.Printf("start7kCenter: %v", err)
					pub.Publish("covis.error", NewOpError(err, sw))
					continue mainloop
				}
			case InvalidTransducer:
				// Need to switch transducers
				log.Println("Allowing 15 seconds for 7kCenter to shutdown")
				if !waitPortClosed(ctx, cfg.Sonar.Address, 15*time.Second) {
					log.Println("7kCenter not shutting down")
					continue mainloop
				}
				// Pause before requesting a restart.
				time.Sleep(serverShutdownTime)
				err = reson.RcmRun(cfg.Rcm.Address, xcodes[sw.Transducer])
				if err != nil {
					log.Printf("RcmRun: %v", err)
					pub.Publish("covis.error", NewOpError(err, sw))
					continue mainloop
				}
				log.Println("Allowing 15 seconds for 7kCenter to start")
				if !waitPortOpen(ctx, cfg.Sonar.Address, 15*time.Second) {
					log.Println("7kCenter not starting")
					pub.Publish("covis.error", NewOpError(fmt.Errorf("7kCenter not starting"), sw))
					continue mainloop
				}
				conn, devs, err = start7kCenter(cfg.Sonar, xnames[sw.Transducer])
				if err != nil {
					log.Printf("start7kCenter: %v", err)
					pub.Publish("covis.error", NewOpError(err, sw))
					continue mainloop
				}
			default:
				log.Print(e)
				continue mainloop
			}
		}

		// Write the Reson transducer XML description to the sweep data directory
		ioutil.WriteFile(
			fmt.Sprintf("%s/transducer.xml", sw.Dir),
			[]byte(devs[0].Details),
			0644)

		var (
			client translator.TranslatorClient
			gconn  *grpc.ClientConn
		)

		client = nil
		gconn = nil

		// Open connection to Translator gRPC server
		if sw.Stops != nil {
			gconn, err = grpcClient(cfg.Translator)
			if err != nil {
				log.Printf("Cannot access gRPC server: %v", err)
				pub.Publish("covis.error", NewOpError(err, sw))
			} else {
				client = translator.NewTranslatorClient(gconn)
			}
		}

		p := platform.New(conn, client, pub)
		err = p.Setup(ctx, sw.Settings)
		if err == nil {
			// Convert the requested starting angles to rotator space
			sw.Stops.Start[translator.PAN] = cfg.Translator.Hdg.Apply(sw.Stops.Start[translator.PAN])
			log.Printf("Target rotator heading: %.1f", sw.Stops.Start[translator.PAN])
			sw.Stops.Start[translator.ROLL] = cfg.Translator.Roll.Apply(sw.Stops.Start[translator.ROLL])
			log.Printf("Target roll angle: %.1f", sw.Stops.Start[translator.ROLL])
			sw.Stops.Start[translator.TILT] = cfg.Translator.Pitch.Apply(sw.Stops.Start[translator.TILT])
			log.Printf("Target tilt angle: %.1f", sw.Stops.Start[translator.TILT])

			err = p.RunSweep(ctx, sw)
			p.Close()

			// Convert back to physical space for logging
			sw.Stops.Start[translator.PAN] = cfg.Translator.Hdg.Invert(sw.Stops.Start[translator.PAN])
			sw.Stops.Start[translator.ROLL] = cfg.Translator.Roll.Invert(sw.Stops.Start[translator.ROLL])
			sw.Stops.Start[translator.TILT] = cfg.Translator.Pitch.Invert(sw.Stops.Start[translator.TILT])

			if err != nil {
				log.Printf("sw.Run error: %v", err)
				pub.Publish("covis.error", NewOpError(err, sw))
				conn.Shutdown()
				conn.Close()
				log.Println("Allowing 15 seconds for 7kCenter to shutdown")
				if !waitPortClosed(ctx, cfg.Sonar.Address, 15*time.Second) {
					log.Println("7kCenter not shutting down")
				}

			} else {
				conn.Close()
				pub.Publish("covis.sweep", sw)
			}
		} else {
			p.Close()
			conn.Close()
			log.Printf("Sonar setup: %v", err)
		}

		if client != nil {
			gconn.Close()
		}

		debug.FreeOSMemory()
	}
}
