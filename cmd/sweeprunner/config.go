package main

import (
	"io/ioutil"

	"github.com/BurntSushi/toml"
	"github.com/imdario/mergo"
	"github.com/pkg/errors"
)

type sysConfig struct {
	Redis      redisConfig `toml:"redis"`
	Sonar      sonarConfig `toml:"sonar"`
	Rcm        rcmConfig   `toml:"rcm"`
	Translator transConfig `toml:"translator"`
}

type rcmConfig struct {
	Address string `toml:"address"`
}

type redisConfig struct {
	Address string `toml:"address"`
	Queue   string `toml:"queue"`
}

type xducerConfig struct {
	Id   string `toml:"id"`
	Name string `toml:"name"`
	Code uint   `toml:"code"`
}

type sonarConfig struct {
	Address    string `toml:"address"`
	Transducer []xducerConfig
}

type transConfig struct {
	Address string        `toml:"address"`
	Hdg     linearMapping `toml:"heading"`
	Pitch   linearMapping `toml:"pitch"`
	Roll    linearMapping `toml:"roll"`
}

type linearMapping struct {
	Offset float64 `toml:"offset"`
	Scale  float64 `toml:"scale"`
}

func (lm linearMapping) Apply(x float64) float64 {
	if lm.Scale == 0.0 {
		return x + lm.Offset
	}
	return x*lm.Scale + lm.Offset
}

func (lm linearMapping) Invert(y float64) float64 {
	if lm.Scale == 0.0 {
		return y - lm.Offset
	}
	return (y - lm.Offset) / lm.Scale
}

var DEFAULT = `
[redis]
# Redis server host and TCP port
address = "localhost:6379"
# Key for list which holds the sweep request queue
queue = "sweeps"
[sonar]
# Reson sonar hostname and TCP port
address = "sonar:7000"
[[sonar.transducer]]
# Transducer ID, name, and RCM code
id = "tc2162_200khz"
name = "7125 (200kHzEA TC2162)"
code = 1
[[sonar.transducer]]
id = "tc2162_400khz"
name = "7125 (400kHzEA)"
code = 2
[[sonar.transducer]]
id = "tc2160_400khz"
name = "7125 (400kHzED)"
code = 3
[rcm]
# Reson Command Manager hostname and TCP port
address = "sonar:50000"
[translator]
# Translator gRPC server host and TCP port
address = "localhost:10130"
# Offset to add to the compass heading to convert
# it to a pan rotator angle.
[translator.heading]
offset = 0.0
scale = 1.0
`

func loadConfiguration(base, cfgfile string, cfg *sysConfig) error {
	var (
		defcfg sysConfig
		err    error
	)

	if err = toml.Unmarshal([]byte(base), &defcfg); err != nil {
		return errors.Wrap(err, "default config")
	}

	if cfgfile != "" {
		b, err := ioutil.ReadFile(cfgfile)
		if err != nil {
			return errors.Wrap(err, "read config file")
		}

		if err = toml.Unmarshal(b, cfg); err != nil {
			return errors.Wrap(err, "parse config file")
		}
	}

	mergo.Merge(cfg, defcfg)

	return nil
}
