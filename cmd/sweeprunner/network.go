package main

import (
	"context"
	"net"
	"time"
)

// Wait for a TCP port to open on a host or for a time limit to expire.
func waitPortOpen(ctx context.Context, address string, limit time.Duration) bool {
	start := time.Now()
	for {
		conn, _ := net.Dial("tcp", address)
		if conn != nil {
			conn.Close()
			return true
		}

		if time.Since(start) > limit {
			break
		}

		select {
		case <-ctx.Done():
			return false
		case <-time.After(500 * time.Millisecond):
		}
	}

	return false
}

// Wait for a TCP port to close on a host or for a time limit to expire.
func waitPortClosed(ctx context.Context, address string, limit time.Duration) bool {
	start := time.Now()
	for {
		conn, _ := net.Dial("tcp", address)
		if conn == nil {
			return true
		}
		conn.Close()

		if time.Since(start) > limit {
			break
		}

		select {
		case <-ctx.Done():
			return false
		case <-time.After(500 * time.Millisecond):
		}
	}

	return false
}
