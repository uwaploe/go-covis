package main

import (
	"testing"
)

func TestMappings(t *testing.T) {
	var table = []struct {
		lm   linearMapping
		x, y float64
	}{
		{x: 42.0, y: 42.0},
		{lm: linearMapping{Offset: -10}, x: 42, y: 32},
		{lm: linearMapping{Offset: 0.5, Scale: 2}, x: 10, y: 20.5},
	}

	for _, e := range table {
		val := e.lm.Apply(e.x)
		if val != e.y {
			t.Errorf("Apply failed; got %v, expected %v", val, e.y)
		}
		val = e.lm.Invert(e.y)
		if val != e.x {
			t.Errorf("Invert failed; got %v, expected %v", val, e.x)
		}
	}
}
