// Runsweep loads a COVIS sweep definition into the task queue read by
// the sweep runner service.
package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"runtime"
	"sort"

	"bitbucket.org/uwaploe/go-covis/pkg/sweep"
	"github.com/BurntSushi/toml"
	"github.com/garyburd/redigo/redis"
	homedir "github.com/mitchellh/go-homedir"
	"github.com/pkg/errors"
	"github.com/spf13/pflag"
	"github.com/spf13/viper"
)

var Version = "dev"
var BuildDate = "unknown"

var usage = `
Usage: runsweep [options] sweeptype [sweeptype ...]

Load one or more sweep definitions into the task queue read by the COVIS
sweep runner service. The "sweeptype" argument specifies the name of a
sweep defined in the sweep configuration file.

`

type sweeps map[string]*sweep.Sweep

func loadSweeps(filename string) (sweeps, error) {
	b, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, errors.Wrap(err, "read sweeps")
	}
	var sw sweeps
	if _, err = toml.Decode(string(b), &sw); err != nil {
		return nil, errors.Wrap(err, "decode sweeps")
	}

	// Assign the type field of each sweep
	for k, _ := range sw {
		sw[k].Type = k
	}

	return sw, nil
}

func main() {
	cfgfile, _ := homedir.Expand("~/config/sweeps.toml")
	flag.String("redis", "localhost:6379", "Redis server address")
	flag.String("queue", "sweeps", "Redis lists containing the task queue")
	flag.String("config", cfgfile, "Sweep configuration file")
	flag.Bool("version", false, "Show program version information and exit")
	flag.Bool("list", false, "List available sweep types and exit")

	pflag.Usage = func() {
		fmt.Fprintf(os.Stderr, usage)
		pflag.PrintDefaults()
	}
	pflag.CommandLine.AddGoFlagSet(flag.CommandLine)
	pflag.Parse()
	viper.BindPFlags(pflag.CommandLine)
	viper.SetEnvPrefix("rs")
	viper.AutomaticEnv()

	if viper.GetBool("version") {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	m, err := loadSweeps(viper.GetString("config"))
	if err != nil {
		log.Fatal(err)
	}

	if viper.GetBool("list") {
		keys := make([]string, 0, len(m))
		for k, _ := range m {
			keys = append(keys, k)
		}
		sort.Strings(keys)
		fmt.Println(keys)
		os.Exit(0)
	}

	for _, swtype := range pflag.Args() {
		conn, err := redis.Dial("tcp", viper.GetString("redis"))
		if err != nil {
			log.Fatalf("Cannot access Redis: %v", err)
		}

		sw, ok := m[swtype]
		if !ok {
			log.Fatalf("Unknown sweep type: %q", swtype)
		}

		b, err := json.Marshal(sw)
		if err != nil {
			log.Fatalf("JSON encode error: %v", err)
		}

		n, err := redis.Int(conn.Do("LPUSH", viper.GetString("queue"), b))
		if err != nil {
			log.Fatalf("Redis LPUSH error: %v", err)
		}
		log.Printf("Task queue length: %d", n)
	}
}
