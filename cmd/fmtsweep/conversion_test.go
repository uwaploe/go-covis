package main

import (
	"bytes"
	"os"
)

var INPUT = `{"type":"target",
"transducer":"tc2160_400khz",
"settings":{"rxgain":40,"txpower":200,"pulse_width":0.001,"range":8},
"motion":{"start":[0,-30,150],"steps":40,"inc":[0,1,0],"roundtrip":false},
"pings":2,
"rate":2}
`

func ExampleConversion() {
	r := bytes.NewReader([]byte(INPUT))
	jsonToToml(r, os.Stdout)

	// Output:
	// [target]
	//   transducer = "tc2160_400khz"
	//   pings = 2
	//   rate = 2.0
	//   [target.settings]
	//     rxgain = 40.0
	//     txpower = 200.0
	//     pw = 0.001
	//     range = 8.0
	//   [target.motion]
	//     start = [0.0, -30.0, 150.0]
	//     inc = [0.0, 1.0, 0.0]
	//     steps = 40
	//     roundtrip = false
}
