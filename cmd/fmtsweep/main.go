// Fmtsweep reads a JSON sweep definition from standard input and writes
// it to standard output in TOML format so it can be addded to the sweep
// configuration file.
package main

import (
	"encoding/json"
	"io"
	"log"
	"os"

	"bitbucket.org/uwaploe/go-covis/pkg/sweep"
	"github.com/BurntSushi/toml"
)

func jsonToToml(rdr io.Reader, w io.Writer) error {
	sw := sweep.Sweep{}

	dec := json.NewDecoder(rdr)
	if err := dec.Decode(&sw); err != nil {
		return err
	}
	sweeps := map[string]*sweep.Sweep{sw.Type: &sw}
	sw.Type = ""
	sw.Dir = ""

	return toml.NewEncoder(w).Encode(&sweeps)
}

func main() {
	if err := jsonToToml(os.Stdin, os.Stdout); err != nil {
		log.Fatal(err)
	}
}
