// Pingtest collects a set of data from the COVIS sonar
package main

import (
	"context"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"os/signal"
	"runtime"
	"syscall"
	"time"

	"bitbucket.org/uwaploe/go-covis/pkg/platform"
	"bitbucket.org/uwaploe/go-covis/pkg/publish/publog"
	"bitbucket.org/uwaploe/go-reson"
	"github.com/BurntSushi/toml"
	"github.com/imdario/mergo"
	"github.com/pkg/errors"
)

type sysConfig struct {
	Sonar  sonarConfig  `toml:"sonar"`
	Sample sampleConfig `toml:"sample"`
}

type sonarConfig struct {
	Address  string            `toml:"address"`
	Settings reson.SonarParams `toml:"settings"`
}

type sampleConfig struct {
	Pings     uint    `toml:"pings"`
	Rate      float32 `toml:"rate"`
	Directory string  `toml:"_"`
}

var Version = "dev"
var BuildDate = "unknown"

var usage = `
Usage: pingtest [options] [datadir]

Collects a set of data from the COVIS sonar.
`

// Default configuration
var DEFAULT = `
[sonar]
# Reson sonar hostname and TCP port
address = "localhost:7000"
[sonar.settings]
# Receiver gain in dB
rxgain = 32.0
# Transmit power level in dB
txpower = 180.0
# Transmit pulse width in seconds
pw = 0.001
# Transmit range in meters
range = 5.0
[sample]
pings = 10
# Ping repetition rate in Hz
rate = 1.0
`

var (
	showvers = flag.Bool("version", false,
		"Show program version information and exit")
	dumpcfg = flag.Bool("dump", false,
		"Dump the default configuration to standard output and exit")
	config = flag.String("config", "", "Configuration file")
	debug  = flag.Bool("debug", false,
		"Output diagnostic information while running")
	timeout = flag.Duration("timeout", 3*time.Second,
		"Maximum time between sonar data packets")
)

func loadConfiguration(base, cfgfile string, cfg *sysConfig) error {
	var (
		defcfg sysConfig
		err    error
	)

	if err = toml.Unmarshal([]byte(base), &defcfg); err != nil {
		return errors.Wrap(err, "default config")
	}

	if cfgfile != "" {
		b, err := ioutil.ReadFile(cfgfile)
		if err != nil {
			return errors.Wrap(err, "read config file")
		}

		if err = toml.Unmarshal(b, cfg); err != nil {
			return errors.Wrap(err, "parse config file")
		}
	}

	mergo.Merge(cfg, defcfg)

	return nil
}

func main() {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, usage)
		flag.PrintDefaults()
	}

	flag.Parse()
	if *showvers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	if *dumpcfg {
		fmt.Printf("%s", DEFAULT)
		os.Exit(0)
	}

	var (
		err error
		cfg sysConfig
	)

	if err = loadConfiguration(DEFAULT, *config, &cfg); err != nil {
		log.Fatal(err)
	}

	args := flag.Args()
	if len(args) == 0 {
		cfg.Sample.Directory = fmt.Sprintf("COVIS-%s-test",
			time.Now().UTC().Format("20060102T150405"))
	} else {
		cfg.Sample.Directory = args[0]
	}

	if *debug {
		reson.LogFunc = log.Printf
	}

	conn, err := reson.Dial("tcp", cfg.Sonar.Address, *timeout)
	if err != nil {
		log.Fatalf("reson.Dial: %v", err)
	}
	defer conn.Close()

	devs, err := conn.GetDevices()
	if err != nil {
		log.Fatalf("GetDevices: %v", err)
	}

	log.Printf("Using transducer %q (%d, %d)",
		devs[0].Name, devs[0].Deviceid, devs[0].Enumerator)
	conn.SetEndpoint(devs[0].Deviceid, devs[0].Enumerator)
	p := platform.New(conn, nil,
		publog.New(log.New(os.Stderr, "", log.LstdFlags)))

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	// Initialize signal handler
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT,
		syscall.SIGTERM, syscall.SIGHUP, syscall.SIGPIPE)
	defer signal.Stop(sigs)

	// Goroutine to wait for signals or for the signal channel
	// to close. When a signal arrives, cancel the Context which
	// will stop the data reading loop.
	go func() {
		s, more := <-sigs
		if more {
			log.Printf("Got signal: %v", s)
			cancel()
		}
	}()

	err = p.Setup(ctx, cfg.Sonar.Settings)
	if err != nil {
		log.Fatal(err)
	}

	if err = os.MkdirAll(cfg.Sample.Directory, 0755); err != nil {
		log.Fatalf("Make data directory: %v", err)
	}
	// Write the Reson transducer XML description to the data directory
	ioutil.WriteFile(
		fmt.Sprintf("%s/transducer.xml", cfg.Sample.Directory),
		[]byte(devs[0].Details),
		0644)

	burst := platform.Burst{
		Pings: cfg.Sample.Pings,
		Rate:  cfg.Sample.Rate,
	}

	t_start := time.Now()
	_, err = p.Collect(ctx, burst, cfg.Sample.Directory, 1)
	if err != nil {
		log.Println(err)
	}

	elapsed := time.Now().Sub(t_start)
	conn.Unsubscribe()
	log.Printf("Elapsed time: %s\n", elapsed)
	log.Printf("Data files are available in: %s\n", cfg.Sample.Directory)
}
