// Readtemp reads the three thermocouples and writes the temperature values
// to standard output in JSON format.
package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"os"
	"strings"
	"time"

	"bitbucket.org/uwaploe/go-covis/pkg/thermocouple"
	"github.com/garyburd/redigo/redis"
	"github.com/spf13/pflag"
	"github.com/spf13/viper"
	"github.com/tarm/serial"
)

// Thermistor board address
const thermAddress uint = 0

// Number of channels on the board
const thermChannels uint = 3

var usage = `Usage: readtemp [options] serialdev

Read the thermocouples through a serial device and write the temperature
values to standard output in JSON format.
`

type DataRecord struct {
	Timestamp time.Time      `json:"t"`
	Data      map[string]int `json:"data"`
}

func main() {
	flag.Int("baud", 9600, "Serial device baud rate")
	flag.Bool("debug", false, "Enable debugging output")
	flag.String("redis", "", "Redis <ADDRESS:PORT,CHANNEL> to publish message")

	pflag.Usage = func() {
		fmt.Fprintf(os.Stderr, usage)
		pflag.PrintDefaults()
	}
	pflag.CommandLine.AddGoFlagSet(flag.CommandLine)
	pflag.Parse()
	viper.BindPFlags(pflag.CommandLine)
	viper.SetEnvPrefix("readtemp")
	viper.AutomaticEnv()

	cfg := &serial.Config{
		Name:        pflag.Arg(0),
		Baud:        viper.GetInt("baud"),
		ReadTimeout: time.Second * 3,
	}
	p, err := serial.OpenPort(cfg)
	if err != nil {
		log.Fatal(err)
	}

	m := thermocouple.NewModule(p, thermAddress, thermChannels)
	m.SetDebug(viper.GetBool("debug"))
	dr := DataRecord{}
	dr.Data = make(map[string]int)
	dr.Timestamp = time.Now().UTC()
	for i := 0; i < int(thermChannels); i++ {
		name := string('A' + byte(i))
		dr.Data[name], err = m.Read(uint(i))
		if err != nil {
			log.Fatalf("Cannot read channel %d: %v", i, err)
		}
	}

	b, err := json.Marshal(dr)
	if err != nil {
		log.Fatalf("JSON serialize: %v", err)
	}

	if rd := viper.GetString("redis"); rd != "" {
		parts := strings.Split(rd, ",")
		if len(parts) == 2 {
			conn, err := redis.Dial("tcp", parts[0])
			if err != nil {
				log.Fatalf("redis connect: %v", err)
			}
			conn.Do("PUBLISH", parts[1], b)
		}
	} else {
		fmt.Printf("%q\n", b)
	}
}
