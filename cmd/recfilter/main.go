// Recfilter sets the data record filter for Reson 7kCenter.
package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
	"time"

	reson "bitbucket.org/uwaploe/go-reson"
	"github.com/spf13/pflag"
	"github.com/spf13/viper"
)

var usage = `Usage: recfilter [options]

Set the data record filter for a Reson 7kCenter recording session.
`

func main() {
	flag.String("reson", "10.95.191.156:7000", "Reson 7kCenter server address")
	flag.String("types", "7000,7038", "Comma separated list of record types")

	pflag.Usage = func() {
		fmt.Fprintf(os.Stderr, usage)
		pflag.PrintDefaults()
	}
	pflag.CommandLine.AddGoFlagSet(flag.CommandLine)
	pflag.Parse()
	viper.BindPFlags(pflag.CommandLine)
	viper.SetEnvPrefix("rf")
	viper.AutomaticEnv()

	conn, err := reson.Dial("tcp", viper.GetString("reson"), time.Second*3)
	if err != nil {
		log.Fatalf("Cannot access server: %v", err)
	}

	rtypes := make([]uint32, 0)
	for _, rtype := range strings.Split(viper.GetString("types"), ",") {
		x, err := strconv.Atoi(rtype)
		if err != nil {
			log.Fatalf("Cannot decode record number: %q ; %v", rtype, err)
		}
		rtypes = append(rtypes, uint32(x))
	}

	err = conn.RecFilter(rtypes)
	if err != nil {
		log.Fatalf("Cannot set filter: %v", err)
	}
	log.Printf("Data record filter set to: %v", rtypes)
}
