// Readbite reads the latest BITE (built-in test environment) data record
// from a Reson 7kCenter server and either writes it to standard output or
// publishes it to a Redis channel.
package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"os"
	"strings"
	"time"

	reson "bitbucket.org/uwaploe/go-reson"
	"github.com/garyburd/redigo/redis"
	"github.com/spf13/pflag"
	"github.com/spf13/viper"
)

var usage = `Usage: readbite [options] HOST:PORT

Reads the latest BITE (built-in test environment) data record from a Reson
7kCenter server and either writes it to standard output or publishes it to
a Redis channel.

`

func main() {
	flag.String("redis", "", "Redis <HOST:PORT,CHANNEL> to publish message")
	flag.Duration("timeout", time.Second*5, "Time allowed for server to respond")

	pflag.Usage = func() {
		fmt.Fprintf(os.Stderr, usage)
		pflag.PrintDefaults()
	}
	pflag.CommandLine.AddGoFlagSet(flag.CommandLine)
	pflag.Parse()
	viper.BindPFlags(pflag.CommandLine)
	viper.SetEnvPrefix("readbite")
	viper.AutomaticEnv()

	conn, err := reson.Dial(pflag.Arg(0), viper.GetDuration("timeout"))
	if err != nil {
		log.Fatalf("7kCenter connect: %v", err)
	}
	defer conn.Close()

	reson.LogFunc = log.Printf

	// devs, err := conn.GetDevices()
	// if err != nil {
	// 	log.Fatalf("GetDevices: %v", err)
	// }
	// conn.SetEndpoint(devs[0].Deviceid, devs[0].Enumerator)

	drf, err := conn.ReqRecord(7021)
	if err != nil {
		log.Fatalf("Request record 7021: %v", err)
	}

	bd, err := reson.ReadBITEData(drf.Data)
	if err != nil {
		log.Fatalf("Parse BITE record: %v", err)
	}

	b, err := json.Marshal(bd)
	if err != nil {
		log.Fatalf("JSON serialize: %v", err)
	}

	if rd := viper.GetString("redis"); rd != "" {
		parts := strings.Split(rd, ",")
		if len(parts) == 2 {
			conn, err := redis.Dial("tcp", parts[0])
			if err != nil {
				log.Fatalf("redis connect: %v", err)
			}
			defer conn.Close()
			conn.Do("PUBLISH", parts[1], b)
		}
	} else {
		fmt.Printf("%q\n", b)
	}
}
