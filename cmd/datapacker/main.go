// Datapacker monitors a Redis channel for messages describing completed
// data collection sweeps, packages the data directory into a compressed
// TAR archive and deletes the directory.
package main

import (
	"context"
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"os"
	"os/exec"
	"os/signal"
	"path/filepath"
	"runtime"
	"syscall"

	"bitbucket.org/uwaploe/go-covis/pkg/sweep"
	"github.com/garyburd/redigo/redis"
	"github.com/spf13/pflag"
	"github.com/spf13/viper"
)

var Version = "dev"
var BuildDate = "unknown"

var usage = `
Usage: datapacker [options] outbox

Monitor a Redis channel for messages describing completed data collection
sweeps, packages the data directory into a compressed TAR archive and
deletes the directory.
`

func dataMon(psc redis.PubSubConn) <-chan string {
	ch := make(chan string, 1)

	go func(debug bool) {
		defer close(ch)
		log.Println("Waiting for messages")
		for {
			switch msg := psc.Receive().(type) {
			case error:
				log.Println(msg)
				return
			case redis.Subscription:
				if msg.Count == 0 {
					log.Println("Pubsub channel closed")
					return
				}
			case redis.Message:
				sw := sweep.Sweep{}
				err := json.Unmarshal(msg.Data, &sw)
				if err != nil {
					log.Printf("JSON decode error: %v", err)
				} else {
					if debug {
						log.Println(sw)
					}
					ch <- sw.Dir
				}
			}
		}

	}(viper.GetBool("debug"))

	return ch
}

func packData(ctx context.Context, datapath, outbox string) {
	datadir := filepath.Base(datapath)
	parent := filepath.Dir(datapath)
	tmpname := filepath.Join(outbox, fmt.Sprintf(".%s.inprogress", datadir))
	arname := filepath.Join(outbox, fmt.Sprintf("%s.tar.gz", datadir))

	cmd := exec.CommandContext(ctx, "tar",
		"-c", "-z", "-f", tmpname, datadir)
	cmd.Dir = parent
	err := cmd.Run()
	if err != nil {
		log.Printf("%v returns error: %v", cmd.Args, err)
		return
	}

	err = os.Rename(tmpname, arname)
	if err != nil {
		log.Printf("Cannot rename %q: %v", tmpname, err)
		return
	}

	log.Printf("New archive: %s", arname)
	if datapath != "/" {
		err = os.RemoveAll(datapath)
		if err != nil {
			log.Printf("Cannot remove %q: %v", datapath, err)
		}
	}
}

func main() {
	flag.String("redis", "localhost:6379", "Redis server address")
	flag.String("channel", "covis.sweep", "Redis channel")
	flag.Bool("version", false, "Show program version information and exit")

	pflag.Usage = func() {
		fmt.Fprintf(os.Stderr, usage)
		pflag.PrintDefaults()
	}
	pflag.CommandLine.AddGoFlagSet(flag.CommandLine)
	pflag.Parse()
	viper.BindPFlags(pflag.CommandLine)
	viper.SetEnvPrefix("dp")
	viper.AutomaticEnv()

	if viper.GetBool("version") {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	if pflag.NArg() == 0 {
		fmt.Fprintln(os.Stderr, "Missing OUTBOX directory argument")
		pflag.Usage()
		os.Exit(1)
	}
	outbox := pflag.Arg(0)

	conn, err := redis.Dial("tcp", viper.GetString("redis"))
	if err != nil {
		log.Fatalf("Cannot access Redis: %v", err)
	}
	psc := redis.PubSubConn{Conn: conn}

	// Initialize signal handler
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT,
		syscall.SIGTERM, syscall.SIGHUP, syscall.SIGPIPE)
	defer signal.Stop(sigs)

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	// Goroutine to wait for signals or for the signal channel
	// to close.
	go func() {
		s, more := <-sigs
		if more {
			log.Printf("Got signal: %v", s)
			psc.Unsubscribe()
			cancel()
		}
	}()

	ch := dataMon(psc)
	psc.Subscribe(viper.GetString("channel"))
	for dataset := range ch {
		go packData(ctx, dataset, outbox)
	}
}
