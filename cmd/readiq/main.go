// Readiq parses a COVIS RawIQ (record type 7038) data file and displays
// the header information.
package main

import (
	"encoding/binary"
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"log"
	"math/cmplx"
	"os"
	"strings"
	"text/template"

	reson "bitbucket.org/uwaploe/go-reson"
)

var usage = `Usage: readiq [options] [rawiqfile]

Read a COVIS RawIQ data record from a file or standard input and write
the header values to standard output as a JSON object.

`
var gnuplotTemplate = `reset
f(x)=20*log10(x+1)
unset key
set xlabel 'Receiver Element'
set ylabel 'Sample'
plot {{printf "%q" .datafile}} binary array=({{.elements}},{{.samples}}) format='%float32' using (f($1)) with image
`
var extractElem int
var ampFile string

func init() {
	flag.IntVar(&extractElem, "extract-elem", -1,
		"extract data from element N to text file")
	flag.StringVar(&ampFile, "amplitude", "", "dump amplitude values to a file")
}

func main() {
	var (
		f   io.Reader
		err error
	)

	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, usage)
		flag.PrintDefaults()
	}

	flag.Parse()
	args := flag.Args()

	if len(args) > 0 {
		f, err = os.Open(args[0])
		if err != nil {
			log.Fatal(err)
		}
	} else {
		f = os.Stdin
	}

	rec, err := reson.ReadRawIQ(f)
	if err != nil {
		log.Fatal(err)
	}

	enc := json.NewEncoder(os.Stdout)
	enc.SetIndent("", "  ")
	err = enc.Encode(rec.Hdr)
	if err != nil {
		log.Fatal(err)
	}

	if extractElem >= 0 {
		outf, err := os.Create(fmt.Sprintf("elem_%d_iq.txt", extractElem))
		if err != nil {
			log.Fatal(err)
		}
		defer outf.Close()

		var cvt func(x uint16) int16
		if rec.Hdr.Samp_type == 16 {
			cvt = func(x uint16) int16 { return int16(x) >> 4 }
		} else {
			cvt = func(x uint16) int16 { return int16(x<<4) >> 4 }
		}

		j := int(0)
		for i := extractElem; j < int(rec.Hdr.Samples); i += int(rec.Hdr.Reduced_elem) {
			fmt.Fprintf(outf, "%d %d\n", cvt(rec.Data[i].I), cvt(rec.Data[i].Q))
			j++
		}
	}

	if ampFile != "" {
		tmpl := template.Must(template.New("gp").Parse(gnuplotTemplate))

		dataf, err := os.Create(ampFile)
		if err != nil {
			log.Fatal(err)
		}
		defer dataf.Close()

		var gpFile string
		if idx := strings.LastIndex(ampFile, "."); idx != -1 {
			gpFile = fmt.Sprintf("%s.gp", ampFile[0:idx])
		} else {
			gpFile = ampFile + ".gp"
		}

		plotf, err := os.Create(gpFile)
		if err != nil {
			log.Fatal(err)
		}
		defer plotf.Close()

		m := make(map[string]interface{})
		m["datafile"] = ampFile
		m["elements"] = int(rec.Hdr.Reduced_elem)
		m["samples"] = int(rec.Hdr.DataSize() / int(rec.Hdr.Reduced_elem))

		log.Printf("Writing amplitude values to %q", ampFile)
		ds := rec.Dataset()
		amp := make([]float32, len(ds))
		for i := 0; i < len(ds); i++ {
			amp[i] = float32(cmplx.Abs(complex128(ds[i])))
		}

		binary.Write(dataf, binary.LittleEndian, amp)
		tmpl.Execute(plotf, m)
		log.Printf("Plot amplitude values with: gnuplot %q\n", gpFile)
	}

}
