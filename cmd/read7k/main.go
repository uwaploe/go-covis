// Read7k lists and extracts the contents of a Reson S7K data file.
package main

import "bitbucket.org/uwaploe/go-covis/cmd/read7k/cmd"

var Version = "dev"
var BuildDate = "unknown"

func main() {
	cmd.Execute(Version)
}
