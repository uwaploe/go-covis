package cmd

import (
	"encoding/json"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"strconv"

	reson "bitbucket.org/uwaploe/go-reson"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

// extractCmd represents the extract command
var extractCmd = &cobra.Command{
	Use:   "extract s7kfile type",
	Short: "Extract the contents to separate files",
	Args:  cobra.ExactArgs(2),
	Run: func(cmd *cobra.Command, args []string) {
		f, err := os.Open(args[0])
		if err != nil {
			fmt.Fprintf(os.Stderr, "Open %s: %v", args[0], err)
			return
		}
		drtype, err := strconv.Atoi(args[1])
		if err != nil {
			fmt.Fprintf(os.Stderr, "Invalid type %q: %v", args[1], err)
			return
		}
		err = extractContents(f, viper.GetString("dir"),
			uint32(drtype), viper.GetBool("json"))
		if err != nil {
			fmt.Fprintf(os.Stderr, "Cannot extract: %v", err)
		}
	},
}

func init() {
	rootCmd.AddCommand(extractCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// extractCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	extractCmd.Flags().BoolP("json", "j", false, "Output data in JSON format")
	extractCmd.Flags().StringP("dir", "d", ".", "Output directory")
	viper.BindPFlag("json", extractCmd.Flags().Lookup("json"))
	viper.BindPFlag("dir", extractCmd.Flags().Lookup("dir"))
}

func extractContents(data io.Reader, dir string, drtype uint32, useJson bool) error {
	counter := int(1)
	for {
		drf, err := reson.ReadDataRecordFrame(data)
		if err == io.EOF {
			break
		}
		if drf.Hdr.Type != drtype {
			continue
		}

		if useJson {
			dr, err := drf.GetDataRecord()
			if err != nil {
				return err
			}
			fname := fmt.Sprintf("rec_%04d_%04d.json", drf.Hdr.Type, counter)
			f, err := os.Create(filepath.Join(dir, fname))
			if err != nil {
				return err
			}
			b, err := json.MarshalIndent(dr, "", "  ")
			if err != nil {
				return err
			}
			f.Write(b)
			f.Close()
		} else {
			fname := fmt.Sprintf("rec_%04d_%04d.bin", drf.Hdr.Type, counter)
			f, err := os.Create(filepath.Join(dir, fname))
			if err != nil {
				return err
			}
			_, err = io.Copy(f, drf.Data)
			f.Close()
			if err != nil {
				return err
			}
		}
		counter++
	}

	return nil
}
