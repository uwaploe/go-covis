package cmd

import (
	"fmt"
	"io"
	"os"
	"text/template"

	reson "bitbucket.org/uwaploe/go-reson"
	"github.com/spf13/cobra"
)

const templateText = `{{.Type}} {{.T | toTime}} {{.Size}}`

func toTime(t reson.Timestamp) string {
	return t.Time().Format("2006-01-02 15:04:05.000")
}

// listCmd represents the list command
var listCmd = &cobra.Command{
	Use:   "list s7kfile",
	Short: "List the file contents",
	Args:  cobra.ExactArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		fm := template.FuncMap{
			"toTime": toTime,
		}
		tmpl, err := template.New("output").Funcs(fm).Parse(templateText)
		if err != nil {
			fmt.Fprintf(os.Stderr, "Template error: %v", err)
			return
		}

		f, err := os.Open(args[0])
		if err != nil {
			fmt.Fprintf(os.Stderr, "Open %s: %v", args[0], err)
			return
		}
		listContents(f, tmpl, os.Stdout)
	},
}

func init() {
	rootCmd.AddCommand(listCmd)
}

func listContents(data io.Reader, tmpl *template.Template, out io.Writer) error {
	for {
		drf, err := reson.ReadDataRecordFrame(data)
		if err == io.EOF {
			break
		}
		if err != nil {
			return err
		}
		tmpl.Execute(out, drf.Hdr)
		fmt.Fprint(out, "\n")
	}

	return nil
}
