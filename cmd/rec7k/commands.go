package main

import (
	"bytes"
	"fmt"
	"strconv"
	"strings"
	"text/template"

	reson "bitbucket.org/uwaploe/go-reson"
	"github.com/pkg/errors"
)

type Commander interface {
	Help() string
	Synopsis() string
	Validate(args []string) error
	Run(args []string) (string, error)
}

type startCommand struct {
	conn reson.Conn
}

func (c *startCommand) Synopsis() string {
	return "start data record recording"
}

func (c *startCommand) Help() string {
	return "usage: start [--filter=N[,M...]] <dirname>"
}

func (c *startCommand) Validate(args []string) error {
	if len(args) < 1 || (strings.HasPrefix(args[0], "-") && len(args) < 2) {
		return errors.New("missing required argument: dirname")
	}

	return nil
}

func (c *startCommand) Run(args []string) (string, error) {
	argindex := int(0)
	rtypes := make([]uint32, 0)

	if strings.HasPrefix(args[0], "--filter=") {
		parts := strings.SplitN(args[0], "=", 2)
		for _, val := range strings.Split(parts[1], ",") {
			num, err := strconv.Atoi(val)
			if err != nil {
				return "", fmt.Errorf("invalid record number: %q", val)
			}
			rtypes = append(rtypes, uint32(num))
		}
		argindex = 1
	} else {
		rtypes = append(rtypes, uint32(7000), uint32(7038))
	}

	err := c.conn.StartRecording(args[argindex], rtypes)
	if err != nil {
		return "", fmt.Errorf("StartRecording command failed: %v", err)
	}

	var text string
	s, err := c.conn.GetStorageStatus()
	if err != nil {
		return "", fmt.Errorf("GetStorageStatus command failed: %v", err)
	}

	text = fmt.Sprintf("Records %d being logged to: %q\n",
		s.IncludedRecords.Vals, s.Hdr.Filename)

	return text, err
}

var statusText = `Mode: {{.Hdr.Mode | printf "%s" }}
Directory: {{.Hdr.Directory | printf "%q"}}
Filename: {{.Hdr.Filename | printf "%q"}}
Range: {{.Hdr.First.Time | printf "%s"}}, {{.Hdr.Last.Time | printf "%s"}}
Records: {{.Hdr.File_records}}
Record Types: {{.IncludedRecords.Vals | printf "%d"}}
`
var statusTemplate *template.Template

func init() {
	statusTemplate = template.Must(template.New("status").Parse(statusText))
}

type statusCommand struct {
	conn reson.Conn
}

func (c *statusCommand) Synopsis() string {
	return "status of data record recording"
}

func (c *statusCommand) Help() string {
	return "usage: status"
}

func (c *statusCommand) Validate(args []string) error {
	return nil
}

func (c *statusCommand) Run(args []string) (string, error) {
	s, err := c.conn.GetStorageStatus()
	if err != nil {
		return "", fmt.Errorf("GetStorageStatus error: %v", err)
	}

	var b bytes.Buffer
	err = statusTemplate.Execute(&b, s)
	return b.String(), err
}

type stopCommand struct {
	conn reson.Conn
}

func (c *stopCommand) Synopsis() string {
	return "stop data record recording"
}

func (c *stopCommand) Help() string {
	return "usage: stop"
}

func (c *stopCommand) Validate(args []string) error {
	return nil
}

func (c *stopCommand) Run(args []string) (string, error) {
	err := c.conn.StopRecording()
	if err != nil {
		return "", fmt.Errorf("StopRecording error: %v", err)
	}
	return "Recording stopped\n", nil
}
