// Rec7k controls data record recording on a Reson 7kCenter server.
package main

import (
	"fmt"
	"io"
	"log"
	"os"
	"strconv"
	"strings"
	"time"

	reson "bitbucket.org/uwaploe/go-reson"
	prompt "github.com/c-bata/go-prompt"
	shellwords "github.com/mattn/go-shellwords"
)

type completer struct {
	cmds     map[string]Commander
	rtypes   []uint32
	dircache []string
}

func (c *completer) complete(d prompt.Document) []prompt.Suggest {
	bc := d.TextBeforeCursor()
	if bc == "" {
		return nil
	}

	args := strings.Split(bc, " ")

	var s []prompt.Suggest
	switch args[0] {
	case "start":
		if len(args) == 2 {
			if strings.HasPrefix(args[1], "-") {
				s = []prompt.Suggest{{Text: "--filter="}}
				if strings.HasSuffix(args[1], "=") || strings.HasSuffix(args[1], ",") {
					s = make([]prompt.Suggest, len(c.rtypes))
					for i, rt := range c.rtypes {
						s[i] = prompt.Suggest{Text: fmt.Sprintf("%d", rt)}
					}
				}
			} else {
				s = make([]prompt.Suggest, len(c.dircache))
				for i, d := range c.dircache {
					s[i] = prompt.Suggest{Text: fmt.Sprintf("%q", d)}
				}
			}
		}

		if len(args) == 3 {
			s = make([]prompt.Suggest, len(c.dircache))
			for i, d := range c.dircache {
				s[i] = prompt.Suggest{Text: fmt.Sprintf("%q", d)}
			}
		}

	case "status":
	default:
		if len(args) == 1 {
			// number of commands + help
			cmdNames := make([]prompt.Suggest, len(c.cmds)+1)
			cmdNames = append(cmdNames,
				prompt.Suggest{Text: "help", Description: "show help message"})
			for name, cmd := range c.cmds {
				cmdNames = append(cmdNames,
					prompt.Suggest{Text: name, Description: cmd.Synopsis()})
			}

			s = cmdNames
		}
	}
	return prompt.FilterHasPrefix(s, d.GetWordBeforeCursor(), true)
}

type executor struct {
	cmds map[string]Commander
}

func (e *executor) execute(l string) {
	if l == "quit" || l == "exit" {
		os.Exit(0)
		return
	}

	// Ignore spaces
	if len(strings.TrimSpace(l)) == 0 {
		return
	}

	parts, err := shellwords.Parse(l)
	if err != nil {
		fmt.Fprintf(os.Stderr, "ERROR: %v\n", err)
		return
	}

	if parts[0] == "help" {
		showHelp(os.Stdout, e.cmds)
		return
	}

	cmd, ok := e.cmds[parts[0]]
	if !ok {
		fmt.Fprintf(os.Stderr, "Unknown command: %q\n", parts[0])
		return
	}

	var args []string
	if len(parts) != 1 {
		if parts[1] == "-h" || parts[1] == "--help" {
			fmt.Fprint(os.Stdout, cmd.Help())
			return
		}
		args = parts[1:]
	}

	if err := cmd.Validate(args); err != nil {
		fmt.Fprintf(os.Stderr, "ERROR: %v\n", err)
		return
	}

	result, err := cmd.Run(args)
	if err != nil {
		fmt.Fprintf(os.Stderr, "ERROR: %v\n", err)
	} else {
		fmt.Fprint(os.Stdout, result)
	}
}

func showHelp(w io.Writer, cmds map[string]Commander) {
	var maxLen int
	// slice of [name, synopsis]
	text := make([][]string, len(cmds))
	for name, cmd := range cmds {
		text = append(text, []string{name, cmd.Synopsis()})
		if len(name) > maxLen {
			maxLen = len(name)
		}
	}

	var cmdText string
	for name, cmd := range cmds {
		cmdText += fmt.Sprintf("  %-"+strconv.Itoa(maxLen)+"s    %s\n",
			name, cmd.Synopsis())
	}

	fmt.Fprintf(w, "Available commands:\n%s", cmdText)
}

func setupRepl(address string) *prompt.Prompt {
	conn, err := reson.Dial("tcp", address, time.Second*3)
	if err != nil {
		log.Fatal(err)
	}
	rtypes, _ := conn.RecordTypes()
	s, _ := conn.GetStorageStatus()

	cmds := map[string]Commander{
		"start":  &startCommand{conn: conn},
		"status": &statusCommand{conn: conn},
		"stop":   &stopCommand{conn: conn},
	}

	executor := &executor{cmds: cmds}
	completer := &completer{cmds: cmds, rtypes: rtypes}
	completer.dircache = make([]string, 0)
	completer.dircache = append(completer.dircache, s.Hdr.Directory.String())

	return prompt.New(
		executor.execute,
		completer.complete,
		prompt.OptionPrefix(fmt.Sprintf("%s> ", address)),
		prompt.OptionTitle("Reson DR Logging"))
}

func main() {
	var p *prompt.Prompt
	if len(os.Args) > 1 {
		p = setupRepl(os.Args[1])
	} else {
		p = setupRepl("10.95.191.156:7000")
	}
	p.Run()
}
