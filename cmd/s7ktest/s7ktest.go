// Test communication with Reson 7kCenter
package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"time"

	"bitbucket.org/uwaploe/go-reson"
)

var (
	sonar   = flag.String("sonar", "10.0.97.106:7000", "Reson sonar address")
	timeout = flag.Duration("timeout", 5*time.Second, "communication timeout")
)

func main() {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, "%s [options]", os.Args[0])
		flag.PrintDefaults()
	}

	flag.Parse()
	reson.LogFunc = log.Printf
	conn, err := reson.Dial("tcp", *sonar, *timeout)
	if err != nil {
		log.Fatal(err)
	}
	defer conn.Close()

	devs, err := conn.GetDevices()
	if err != nil {
		log.Fatal(err)
	}

	if len(devs) == 0 {
		log.Printf("No devices!")
	} else {
		log.Print(devs[0])
	}

}
