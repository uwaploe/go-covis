// Package pubnats publishes messages (asynchronously) to a NATS server.
package pubnats

import (
	"encoding/json"

	nats "github.com/nats-io/go-nats"
)

type Publisher struct {
	conn *nats.Conn
	ch   chan message
}

type message struct {
	topic string
	data  interface{}
}

func (p *Publisher) listen() {
	for msg := range p.ch {
		b, err := json.Marshal(msg.data)
		if err == nil {
			p.conn.Publish(msg.topic, b)
		}
	}
}

// Publish implements the Publisher interface.
func (p *Publisher) Publish(topic string, data interface{}) {
	p.ch <- message{topic: topic, data: data}
}

// New creates a new Publisher which uses a NATS server to publish
// JSON encoded messages.
func New(conn *nats.Conn) *Publisher {
	p := &Publisher{
		conn: conn,
		ch:   make(chan message, 1),
	}

	go p.listen()

	return p
}
