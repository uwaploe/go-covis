// Package publish provides an interface for publishing messages through
// a message broker
package publish

type Publisher interface {
	// Asynchronously publish a message to a topic
	Publish(topic string, data interface{})
}

type SyncPublisher interface {
	// Asynchronously publish a message to a topic
	SyncPublish(topic string, data interface{}) error
}
