// Package pubredis publishes messages (asynchronously) to a Redis pub-sub server.
package pubredis

import (
	"encoding/json"

	"github.com/garyburd/redigo/redis"
)

type Publisher struct {
	conn redis.Conn
	ch   chan message
}

type message struct {
	topic string
	data  interface{}
}

func (p *Publisher) listen() {
	for msg := range p.ch {
		b, err := json.Marshal(msg.data)
		if err == nil {
			p.conn.Do("PUBLISH", msg.topic, b)
		}
	}
}

// Publish implements the Publisher interface.
func (p *Publisher) Publish(topic string, data interface{}) {
	p.ch <- message{topic: topic, data: data}
}

// New creates a new Publisher which uses a Redis data-store to publish
// JSON encoded messages.
func New(conn redis.Conn) *Publisher {
	p := &Publisher{
		conn: conn,
		ch:   make(chan message, 1),
	}

	go p.listen()

	return p
}
