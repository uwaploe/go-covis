// Package pubampq publishes messages (asynchronously) to an AMQP server
package pubamqp

import (
	"encoding/json"
	"time"

	"github.com/pkg/errors"
	"github.com/streadway/amqp"
)

type Publisher struct {
	conn     *amqp.Connection
	channel  *amqp.Channel
	exchange string
	ch       chan message
}

type message struct {
	topic string
	data  interface{}
}

func (p *Publisher) listen() {
	var (
		ap  amqp.Publishing
		err error
	)

	ap.ContentType = "application/json"
	ap.DeliveryMode = amqp.Transient

	for msg := range p.ch {
		ap.Body, err = json.Marshal(msg.data)
		if err == nil {
			state, err := p.channel.QueueDeclare(
				msg.topic, // our queue name
				true,      // durable
				false,     // delete when unused
				false,     // exclusive
				false,     // no-wait
				nil,       // arguments
			)
			if err == nil {
				ap.Timestamp = time.Now().UTC()
				p.channel.Publish(
					p.exchange,
					state.Name,
					false, // mandatory
					false, // immediate
					ap)
			}
		}
	}
}

// Publish implements the Publisher interface.
func (p *Publisher) Publish(topic string, data interface{}) {
	p.ch <- message{topic: topic, data: data}
}

// New creates a new Publisher which uses an AMQP message broker to publish
// JSON encoded messages.
func New(conn *amqp.Connection, exchange, exchangeType string) (*Publisher, error) {
	p := &Publisher{}
	var err error

	p.conn = conn
	p.channel, err = p.conn.Channel()
	if err != nil {
		return nil, errors.Wrap(err, "open channel")
	}

	if err = p.channel.ExchangeDeclare(
		exchange,     // name of the exchange
		exchangeType, // type
		true,         // durable
		false,        // delete when complete
		false,        // internal
		false,        // noWait
		nil,          // arguments
	); err != nil {
		return nil, errors.Wrap(err, "exchange declare")
	}

	p.exchange = exchange

	go p.listen()

	return p, nil
}
