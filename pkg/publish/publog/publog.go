// Package publog publishes messages (asynchronously) to a log.Logger.
package publog

import (
	"encoding/json"
	"log"
)

type Publisher struct {
	lg *log.Logger
	ch chan message
}

type message struct {
	topic string
	data  interface{}
}

func (p *Publisher) listen() {
	for msg := range p.ch {
		b, err := json.Marshal(msg.data)
		if err == nil {
			p.lg.SetPrefix(msg.topic + " ")
			p.lg.Printf("%s", b)
		}
	}
}

// Publish implements the Publisher interface.
func (p *Publisher) Publish(topic string, data interface{}) {
	p.ch <- message{topic: topic, data: data}
}

// New creates a new Publisher which outputs JSON encoded messages to
// a log.Logger.
func New(lg *log.Logger) *Publisher {
	p := &Publisher{
		lg: lg,
		ch: make(chan message, 1),
	}

	go p.listen()

	return p
}
