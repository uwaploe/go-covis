// Package thermocouple is used to read temperature values from a Weeder
// Technologies Thermocouple input board.
package thermocouple

import (
	"bytes"
	"errors"
	"fmt"
	"io"
	"log"
)

// Module represents a Thermocouple input module
type Module struct {
	port   io.ReadWriter
	addr   byte
	nchans uint
	debug  bool
}

// NewModule creates a new Module at the given address (0-31) with the
// given number of channels (1-4).
func NewModule(rw io.ReadWriter, addr, chans uint) *Module {
	return &Module{
		port:   rw,
		addr:   'A' + byte(addr),
		nchans: chans,
	}
}

// SetDebug enables or disables I/O debugging
func (m *Module) SetDebug(state bool) {
	m.debug = state
}

func readUntil(rdr io.Reader, c byte) ([]byte, error) {
	var (
		buf bytes.Buffer
		err error
		n   int
	)

	b := make([]byte, 1)
	for {
		n, err = rdr.Read(b)
		if n > 0 {
			buf.Write(b[0:n])
		}
		if err != nil || c == b[0] {
			break
		}
	}

	return buf.Bytes(), err
}

// Read returns the temperature value in degrees and any error that
// occcurs.
func (m *Module) Read(channel uint) (int, error) {
	var val int

	if channel >= m.nchans {
		return val, errors.New("Invalid channel")
	}

	cmd := fmt.Sprintf("%cR%c\r", m.addr, 'A'+byte(channel))
	if m.debug {
		log.Printf("< %q\n", cmd)
	}
	m.port.Write([]byte(cmd))
	b, err := readUntil(m.port, '\r')

	if err != nil {
		return val, err
	}

	if m.debug {
		log.Printf("> %q\n", b)
	}

	_, err = fmt.Sscanf(string(b[1:]), "%d\r", &val)
	return val, err
}
