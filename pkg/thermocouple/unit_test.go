package thermocouple

import (
	"bytes"
	"testing"
)

type rwbuf struct {
	rb *bytes.Buffer
	wb bytes.Buffer
}

var RW = &rwbuf{}

func (b *rwbuf) Read(p []byte) (int, error) {
	return b.rb.Read(p)
}

func (b *rwbuf) Write(p []byte) (int, error) {
	return b.wb.Write(p)
}

func init() {
	RW.rb = bytes.NewBufferString("A42\rA-42\r")
}

func TestRead(t *testing.T) {
	m := NewModule(RW, 0, 3)
	val, err := m.Read(4)
	if err == nil {
		t.Errorf("Expected error, got %v", val)
	}
	val, err = m.Read(0)
	if val != 42 {
		t.Errorf("Wrong value, expected 42, got %v", val)
	}
	val, err = m.Read(0)
	if val != -42 {
		t.Errorf("Wrong value, expected -42, got %v", val)
	}
}
