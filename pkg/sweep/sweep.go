// Package sweep provides functions and data structures for COVIS data collection
// sweeps. A sweep is defined as a sequence of one or more pings collected at one
// or more platform positions.
package sweep

import (
	"fmt"
	"time"

	reson "bitbucket.org/uwaploe/go-reson"
)

const (
	ROLL uint = iota
	PITCH
	HEADING
)

// Sweep types
const (
	TypeImaging = "imaging"
	TypeDoppler = "doppler"
	TypeDiffuse = "diffuse"
	TypeSpecial = "special"
	TypeTest    = "test"
)

// Timing is used to calculate the average data collection time for
// each step in a Sweep
type Timing struct {
	count int64
	sum   time.Duration
}

// Motion describes the motion of the platform
type Motion struct {
	// Starting angles [ROLL, PITCH, HEADING]
	Start []float64 `json:"start" toml:"start"`
	// Angle increment at each step
	Increment []float64 `json:"inc" toml:"inc"`
	// Step count
	Steps int `json:"steps" toml:"steps"`
	// If true, return to the starting orientation
	Roundtrip bool `json:"roundtrip" toml:"roundtrip"`
	// If true, level the platform at the start of the sweep
	Level bool `json:"level" toml:"level"`
}

// Sweep describes a data collection sweep
type Sweep struct {
	Type       string            `json:"type" toml:"type,omitempty"`
	Transducer string            `json:"transducer" toml:"transducer"`
	Settings   reson.SonarParams `json:"settings" toml:"settings"`
	Stops      *Motion           `json:"motion,omitempty" toml:"motion,omitempty"`
	Pings      uint              `json:"pings" toml:"pings"`
	Rate       float32           `json:"rate" toml:"rate"`
	Dir        string            `json:"dir,omitempty" toml:"dir,omitempty"`
	stats      *Timing           `json:"-" toml:"-"`
	Tstart     time.Time         `json:"t_start,omitempty" toml:"-"`
	Tend       time.Time         `json:"t_end,omitempty" toml:"-"`
}

func (t *Timing) Update(d time.Duration) {
	t.count++
	t.sum += d
}

func (t Timing) String() string {
	mean := time.Duration(t.sum.Nanoseconds() / t.count)
	return fmt.Sprintf("{\"steps\": %d, \"meanTime\": \"%v\"}", mean)
}
