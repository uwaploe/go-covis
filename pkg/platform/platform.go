// Package platform models the entire COVIS data collection system
package platform

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"path"
	"runtime/debug"
	"sync"
	"time"

	"bitbucket.org/uwaploe/go-covis/pkg/publish"
	"bitbucket.org/uwaploe/go-covis/pkg/sweep"
	"bitbucket.org/uwaploe/go-covis/pkg/translator"
	reson "bitbucket.org/uwaploe/go-reson"
	"github.com/pkg/errors"
)

// Header for the sweep index CSV file
const INDEXHDR = "ping,seconds,usecs,pitch,roll,yaw,kPAngle,kRAngle,kHeading"

// Topics for published events
const (
	TopicMove = "covis.move"
	TopicPing = "covis.ping"
)

// Number of platform leveling attempts
const LevelAttempts int = 2

// Delay time before leveling to allow platform to settle
const LevelDelay time.Duration = time.Second

// Minimum time to wait between StopPing and StartPing commands to 7kCenter
const sampleDelay time.Duration = time.Millisecond * 4500

// Angle represents a rotation of the platform
type Angle struct {
	Roll    float64 `json:"roll"`
	Pitch   float64 `json:"pitch"`
	Heading float64 `json:"heading"`
}

// ToPb converts an Angle to a Protocol Buffer message format
func (a *Angle) ToPb() *translator.Orientation {
	return &translator.Orientation{
		Roll:    int32(a.Roll * translator.ANGLESCALE),
		Pitch:   int32(a.Pitch * translator.ANGLESCALE),
		Heading: int32(a.Heading * translator.ANGLESCALE),
	}
}

// State contains the current orientation state of the platform
type State struct {
	T time.Time `json:"t"`
	// Angle of each Rotator in their own coordinate system
	Rotation [3]float64 `json:"rotation"`
	Stalled  uint       `json:"stall_flag"`
	Angle
}

// Convert a rotation array to a Protocol Buffer Rotation message
func RotationPb(r [3]float64) *translator.Rotation {
	return &translator.Rotation{
		Roll: int32(r[0] * translator.ANGLESCALE),
		Tilt: int32(r[1] * translator.ANGLESCALE),
		Pan:  int32(r[2] * translator.ANGLESCALE),
	}
}

// StateFromPb creates a State struct from a Protocol Buffer message
func StateFromPb(msg *translator.State) *State {
	if msg == nil {
		return nil
	}
	s := State{}
	s.T = time.Now().UTC()
	if msg.R != nil {
		s.Rotation[0] = float64(msg.R.Roll) / translator.ANGLESCALE
		s.Rotation[1] = float64(msg.R.Tilt) / translator.ANGLESCALE
		s.Rotation[2] = float64(msg.R.Pan) / translator.ANGLESCALE
		s.Stalled = uint(msg.R.Stalled)
	}
	if msg.O != nil {
		s.Roll = float64(msg.O.Roll) / translator.ANGLESCALE
		s.Pitch = float64(msg.O.Pitch) / translator.ANGLESCALE
		s.Heading = float64(msg.O.Heading) / translator.ANGLESCALE
	}

	return &s
}

// Burst describes a set of acoustic transmissions
type Burst struct {
	Pings uint
	Rate  float32
}

// Platform represents the COVIS data collection system
type Platform interface {
	// Level the transducer head
	Level(ctx context.Context) (*State, error)
	// Move the transducer to a relative orientation
	Move(ctx context.Context, a *Angle) (*State, error)
	// Move the transducer to an absolute rotator orientation
	Goto(ctx context.Context, rangle [3]float64) (*State, error)
	// Return the current orientation
	State(ctx context.Context) (*State, error)
	// Load the sonar parameters
	Setup(ctx context.Context, settings reson.SonarParams) error
	// Collect and store an acoustic data-set and returns a slice of transmission
	// times for each ping. Burst describes the ping schedule, dir is the data
	// storage directory and index is the starting ping index.
	Collect(ctx context.Context, burst Burst, dir string, index uint) ([]time.Time, error)
	// Run a data collection sweep. A sweep consists of a data-set taken at
	// one or more platform orientations.
	RunSweep(ctx context.Context, sw *sweep.Sweep) error
	// Close the data stream connection to the sonar
	Close()
}

// Concrete type implementing the Platform interface
type platform struct {
	conn, stream reson.Conn
	tr           translator.TranslatorClient
	pub          publish.Publisher
}

// New creates a new Platform from a Reson sonar, a gRPC Translator client,
// and a Publisher.
func New(conn reson.Conn, client translator.TranslatorClient,
	pub publish.Publisher) Platform {
	p := &platform{
		conn: conn,
		tr:   client,
		pub:  pub,
	}
	p.stream, _ = conn.Clone()
	return p
}

func (p *platform) Close() {
	p.stream.Close()
}

func (p *platform) Collect(pctx context.Context, burst Burst,
	dir string, index uint) ([]time.Time, error) {
	var (
		wg  sync.WaitGroup
		err error
	)

	ts := make([]time.Time, burst.Pings)
	ctx, cancel := context.WithCancel(pctx)
	defer cancel()

	counters := map[uint32]uint{
		reson.RTSettings: 0,
		reson.RTRawiq:    0,
	}

	if err = p.stream.Subscribe([]uint32{7000, 7038}); err != nil {
		p.stream.Unsubscribe()
		p.stream.Close()
		p.stream, _ = p.conn.Clone()
		if err = p.stream.Subscribe([]uint32{7000, 7038}); err != nil {
			return ts, errors.Wrap(err, "subscribe")
		}
	}
	defer p.stream.Unsubscribe()

	debug.SetGCPercent(-1)
	defer debug.SetGCPercent(100)

	ch, err := p.stream.Stream(ctx)
	if err != nil {
		return ts, err
	}

	if err = p.conn.Start(burst.Rate); err != nil {
		return ts, errors.Wrap(err, "start")
	}
	defer p.conn.Stop()

loop:
	for drf := range ch {
		code := drf.Hdr.Type
		switch code {
		case reson.RTSettings:
			// Extract data section and save to directory as a
			// JSON file
			if counters[code] < burst.Pings {
				wg.Add(1)
				go func(idx, base uint) {
					defer func() {
						if x := recover(); x != nil {
							log.Printf("PANIC: %v", x)
						}
					}()
					defer wg.Done()
					s := &reson.Settings{}
					err := s.Unpack(drf.Data)
					if err == nil {
						path := fmt.Sprintf("%s/rec_%d_%06d.json", dir, code, idx+base)
						f, err := os.OpenFile(path, os.O_WRONLY|os.O_CREATE, 0644)
						if err == nil {
							defer f.Close()
							b, err := json.MarshalIndent(s, "", "    ")
							if err == nil {
								f.Write(b)
							}

						} else {
							log.Printf("Error opening %q: %v", path, err)
						}
					} else {
						log.Printf("Error unpacking DRF: %v", err)
					}
				}(counters[code], index)
			}
		case reson.RTRawiq:
			// Extract data section and save to directory as a
			// raw binary file.
			p.pub.Publish(TopicPing, drf.Hdr)
			if counters[code] < burst.Pings {
				wg.Add(1)
				go func(idx, base uint) {
					defer func() {
						if x := recover(); x != nil {
							log.Printf("PANIC: %v", x)
						}
					}()
					defer wg.Done()
					path := fmt.Sprintf("%s/rec_%d_%06d.bin", dir, code, idx+base)
					f, err := os.OpenFile(path, os.O_WRONLY|os.O_CREATE, 0644)
					if err == nil {
						defer f.Close()
						_, err = f.Write(drf.Data.Bytes())
						if err != nil {
							log.Printf("Error writing %q: %v", path, err)
						}
						ts[idx] = drf.Hdr.T.Time()
					} else {
						log.Printf("Error opening %q: %v", path, err)
					}
				}(counters[code], index)

			} else {
				break loop
			}
		}

		if counters[reson.RTRawiq] == (burst.Pings - 1) {
			break loop
		}
		counters[code]++
	}

	cancel()
	if err = p.conn.Stop(); err != nil {
		err = errors.Wrap(err, "stop pinging")
	}
	log.Println("Waiting for file writing to finish ...")
	wg.Wait()
	log.Printf("Counters: %#v\n", counters)
	return ts, err
}

type stateFunc func() (*translator.State, error)

// Monitor translator motion and return the final state
func motionMonitor(ctx context.Context, next stateFunc,
	pub publish.Publisher) (*translator.State, error) {
	var (
		state *translator.State
		err   error
	)

	for {
		state, err = next()
		if err == io.EOF {
			break
		}
		if err != nil {
			return nil, err
		}
		select {
		case <-ctx.Done():
			pub.Publish(TopicMove, "Interrupted")
			return state, ctx.Err()
		default:
		}
		if state != nil {
			pub.Publish(TopicMove, StateFromPb(state))
		}
	}

	return state, nil
}

func (p *platform) State(ctx context.Context) (*State, error) {
	state, err := p.tr.GetState(ctx, &translator.Empty{})
	return StateFromPb(state), err
}

func (p *platform) Move(ctx context.Context, a *Angle) (*State, error) {
	if p.tr == nil {
		return nil, errors.New("No rotator access")
	}

	o := a.ToPb()
	stream, err := p.tr.Move(ctx, o)
	if err != nil {
		return nil, errors.Wrap(err, "start move")
	}
	_, err = motionMonitor(ctx, stream.Recv, p.pub)
	if err != nil {
		return nil, errors.Wrap(err, "move")
	}

	return p.State(ctx)
}

func (p *platform) Goto(ctx context.Context, rangles [3]float64) (*State, error) {
	if p.tr == nil {
		return nil, errors.New("No rotator access")
	}

	r := RotationPb(rangles)
	stream, err := p.tr.Goto(ctx, r)
	if err != nil {
		return nil, errors.Wrap(err, "start goto")
	}
	_, err = motionMonitor(ctx, stream.Recv, p.pub)
	if err != nil {
		return nil, errors.Wrap(err, "goto")
	}

	return p.State(ctx)
}

func (p *platform) Level(ctx context.Context) (*State, error) {
	if p.tr == nil {
		return nil, errors.New("No rotator access")
	}

	empty := &translator.Empty{}
	for i := 0; i < LevelAttempts; i++ {
		time.Sleep(LevelDelay)
		stream, err := p.tr.Level(ctx, empty)
		if err != nil {
			return nil, errors.Wrap(err, "level")
		}
		_, err = motionMonitor(ctx, stream.Recv, p.pub)
		if err != nil {
			return nil, errors.Wrap(err, "move")
		}
	}

	return p.State(ctx)
}

func (p *platform) Setup(ctx context.Context, settings reson.SonarParams) error {
	var err error

	if err = p.conn.RawMode(true); err != nil {
		return errors.Wrap(err, "RawMode")
	}

	if err = p.conn.SetParams(settings); err != nil {
		return errors.Wrap(err, "SetParams")
	}

	return nil
}

// Write an entry to the sweep index file
func writeIndex(w io.Writer, ping uint, t time.Time, state *State) {
	fmt.Fprintf(w, "%d,%d,%d,%.1f,%.1f,%.1f,%.1f,%.1f,%.1f\n",
		ping, t.Unix(), t.Nanosecond()/1000,
		state.Rotation[translator.TILT],
		state.Rotation[translator.ROLL],
		state.Rotation[translator.PAN],
		state.Pitch,
		state.Roll,
		state.Heading)
}

// Write the sweep description file
func writeDescription(sw *sweep.Sweep) error {

	v := struct {
		Mode      string            `json:"mode"`
		Id        string            `json:"_id"`
		Timestamp []int64           `json:"starttime"`
		Endtime   []int64           `json:"endtime"`
		Stops     *sweep.Motion     `json:"motion"`
		Settings  reson.SonarParams `json:"settings"`
	}{
		Mode:      sw.Type,
		Timestamp: []int64{sw.Tstart.Unix(), int64(sw.Tstart.Nanosecond()) / 1000},
		Endtime:   []int64{sw.Tend.Unix(), int64(sw.Tend.Nanosecond()) / 1000},
		Id:        path.Base(sw.Dir),
		Stops:     sw.Stops,
		Settings:  sw.Settings,
	}
	b, err := json.MarshalIndent(&v, "", "    ")
	if err == nil {
		ioutil.WriteFile(fmt.Sprintf("%s/sweep.json", sw.Dir), b, 0644)
	}
	return err
}

func (p *platform) avgHeading(ctx context.Context, n int) (float64, error) {
	val := float64(0)
	for i := 0; i < n; i++ {
		state, err := p.State(ctx)
		if err != nil {
			return 0, err
		}
		val += state.Heading
	}
	return val / float64(n), nil
}

func (p *platform) RunSweep(ctx context.Context, sw *sweep.Sweep) error {
	var (
		err   error
		state *State
	)

	if p.tr == nil && sw.Stops != nil {
		return errors.New("No rotator access")
	}

	// Open index file and write header
	path := fmt.Sprintf("%s/index.csv", sw.Dir)
	idxfile, err := os.OpenFile(path, os.O_WRONLY|os.O_CREATE, 0644)
	if err != nil {
		return errors.Wrap(err, "open index")
	}
	defer idxfile.Close()
	fmt.Fprintln(idxfile, INDEXHDR)

	state, err = p.State(ctx)
	if err != nil {
		return err
	}

	// Move to the starting orientation
	if sw.Stops != nil {
		// Calculate heading change in rotator space because the compass
		// heading is too noisy to use.
		dh := sw.Stops.Start[translator.PAN] - state.Rotation[translator.PAN]
		state, err = p.Move(ctx, &Angle{Heading: dh})
		if err != nil {
			return err
		}

		// Level the platform
		if sw.Stops.Level {
			state, err = p.Level(ctx)
			if err != nil {
				return err
			}

			writeIndex(idxfile, 0, time.Now(), state)
			p.pub.Publish("covis.level", state)
		}

		// Adjust pitch angle in rotator space
		var r [3]float64
		// Do not adjust pan/roll, only tilt
		r[translator.PAN] = -1
		r[translator.ROLL] = -1
		r[translator.TILT] = sw.Stops.Start[translator.TILT]
		state, err = p.Goto(ctx, r)
		if err != nil {
			return err
		}

		// Adjust roll angle in rotator space
		r[translator.PAN] = -1
		r[translator.ROLL] = sw.Stops.Start[translator.ROLL]
		r[translator.TILT] = -1
		state, err = p.Goto(ctx, r)
		if err != nil {
			return err
		}

	}

	burst := Burst{Pings: sw.Pings, Rate: sw.Rate}
	p_start := uint(1)
	sw.Tstart = time.Now()
	ts, err := p.Collect(ctx, burst, sw.Dir, p_start)
	if err != nil {
		return err
	}
	t_next := time.Now().Add(sampleDelay)

	if state != nil {
		// Write index entries
		for i, t := range ts {
			if !t.IsZero() {
				writeIndex(idxfile, p_start+uint(i), t, state)
			}
		}
	}

	if sw.Stops != nil {
		p_start += sw.Pings
		angle := Angle{
			Roll:    sw.Stops.Increment[translator.ROLL],
			Pitch:   sw.Stops.Increment[translator.PITCH],
			Heading: sw.Stops.Increment[translator.HEADING],
		}

		for i := 1; i < sw.Stops.Steps; i++ {
			state, err = p.Move(ctx, &angle)
			if err != nil {
				return err
			}
			time.Sleep(t_next.Sub(time.Now()))
			ts, err = p.Collect(ctx, burst, sw.Dir, p_start)
			if err != nil {
				return err
			}
			t_next = time.Now().Add(sampleDelay)

			// Write index entries
			for i, t := range ts {
				if !t.IsZero() {
					writeIndex(idxfile, p_start+uint(i), t, state)
				}
			}

			p_start += sw.Pings
		}

		if sw.Stops.Roundtrip {
			angle := Angle{
				Roll:    -1 * sw.Stops.Increment[translator.ROLL],
				Pitch:   -1 * sw.Stops.Increment[translator.PITCH],
				Heading: -1 * sw.Stops.Increment[translator.HEADING],
			}

			for i := 1; i < sw.Stops.Steps; i++ {
				state, err = p.Move(ctx, &angle)
				if err != nil {
					return err
				}
				time.Sleep(t_next.Sub(time.Now()))
				ts, err = p.Collect(ctx, burst, sw.Dir, p_start)
				if err != nil {
					return err
				}
				t_next = time.Now().Add(sampleDelay)

				// Write index entries
				for i, t := range ts {
					if !t.IsZero() {
						writeIndex(idxfile, p_start+uint(i), t, state)
					}
				}

				p_start += sw.Pings
			}
		}
	}
	sw.Tend = time.Now()
	writeDescription(sw)

	return nil
}
