package translator

import (
	"io"
	"testing"

	context "golang.org/x/net/context"
)

func TestMotion(t *testing.T) {
	var (
		state *State
		err   error
	)

	mt := NewMockTranslator()

	o := Orientation{
		Pitch: 2 * int32(ANGLESCALE),
	}
	stream, err := mt.Move(context.Background(), &o)
	if err != nil {
		t.Fatal(err)
	}
	count := int(0)
	for {
		state, err = stream.Recv()
		if err == io.EOF {
			break
		}
		count++
	}
	if *(state.O) != o {
		t.Errorf("Bad final orientation: %+v", *(state.O))
	}
	if count < 4 {
		t.Errorf("Unexpected count; %d", count)
	}
}
