// Package translator provides a gRPC client to access the COVIS rotators
// and orientation sensor.
package translator

import (
	"io"
	"time"

	proto "github.com/golang/protobuf/proto"
	context "golang.org/x/net/context"
	grpc "google.golang.org/grpc"
)

// Scale factor for angle measurements in Protobuf Messages
const ANGLESCALE float64 = 1e3

// Scale factor for rotator control variables (speed and acceleration)
const CTLSCALE float64 = 1e3

// Convert orientation sensor roll angle change to rotator roll angle
// change
const ROLL2ROLL float64 = 6

// Convert orientation sensor pitch angle change to rotator tilt angle
// change
const PITCH2TILT float64 = -1

// Convert orientation sensor heading angle change to rotator pan angle
// change
const HDG2PAN float64 = 1

const (
	ROLL    int = 0
	PITCH   int = 1
	TILT    int = 1
	HEADING int = 2
	PAN     int = 2
)

type MockTranslator struct {
	state *State
	mover *MockMover
	ctl   *Control
}

type MockMover struct {
	state, target *State
	ctl           *Control
	t             time.Time
	interval      time.Duration
	direction     [3]int32
	grpc.ClientStream
}

func iabs(a int32) int32 {
	if a < 0 {
		a = -a
	}
	return a
}

func NewMockTranslator() *MockTranslator {
	mt := &MockTranslator{}
	mt.state = &State{}
	mt.state.O = &Orientation{}
	mt.state.R = &Rotation{
		Tilt: int32(220 * ANGLESCALE),
		Roll: int32(198.3 * ANGLESCALE),
		Pan:  int32(135 * ANGLESCALE)}
	mt.ctl = &Control{
		Speed: int32(1 * ANGLESCALE),
		Accel: int32(0.5 * ANGLESCALE),
		Brake: 89,
	}
	return mt
}

func (m *MockMover) Recv() (*State, error) {
	next := m.t.Add(m.interval)
	div := int32(time.Second / m.interval)
	time.Sleep(next.Sub(m.t))
	m.t = next
	count := int(0)
	if m.state.O.Roll != m.target.O.Roll {
		count++
		da := m.direction[0] * (m.ctl.Speed / div)
		togo := m.target.O.Roll - m.state.O.Roll
		if iabs(da) > iabs(togo) {
			da = togo
		}
		m.state.O.Roll += da
		m.state.R.Roll += (da * int32(ROLL2ROLL))
	}
	if m.state.O.Pitch != m.target.O.Pitch {
		count++
		da := m.direction[1] * (m.ctl.Speed / div)
		togo := m.target.O.Pitch - m.state.O.Pitch
		if iabs(da) > iabs(togo) {
			da = togo
		}
		m.state.O.Pitch += da
		m.state.R.Tilt += (da * int32(PITCH2TILT))
	}
	if m.state.O.Heading != m.target.O.Heading {
		count++
		da := m.direction[2] * (m.ctl.Speed / div)
		togo := m.target.O.Heading - m.state.O.Heading
		if iabs(da) > iabs(togo) {
			da = togo
		}
		m.state.O.Heading += da
		m.state.R.Pan += (da * int32(HDG2PAN))
	}

	s := proto.Clone(m.state).(*State)
	if count == 0 {
		return s, io.EOF
	}

	return s, nil
}

func (mt *MockTranslator) GetState(_ context.Context,
	in *Empty, opts ...grpc.CallOption) (*State, error) {
	s := proto.Clone(mt.state).(*State)
	return s, nil
}

func (mt *MockTranslator) GetRotation(_ context.Context,
	in *Empty, opts ...grpc.CallOption) (*Rotation, error) {
	r := proto.Clone(mt.state.R).(*Rotation)
	return r, nil
}

func (mt *MockTranslator) GetAttitude(_ context.Context,
	in *Empty, opts ...grpc.CallOption) (*Orientation, error) {
	o := proto.Clone(mt.state.O).(*Orientation)
	return o, nil
}

func (mt *MockTranslator) Move(_ context.Context,
	in *Orientation, opts ...grpc.CallOption) (Translator_MoveClient, error) {
	m := MockMover{}
	m.state = proto.Clone(mt.state).(*State)
	m.target = &State{}
	m.target.O = proto.Clone(in).(*Orientation)
	m.target.R = &Rotation{}
	m.t = time.Now()
	m.interval = 500 * time.Millisecond
	m.ctl = proto.Clone(mt.ctl).(*Control)
	if m.target.O.Roll > m.state.O.Roll {
		m.direction[0] = 1
	} else {
		m.direction[0] = -1
	}
	if m.target.O.Pitch > m.state.O.Pitch {
		m.direction[1] = 1
	} else {
		m.direction[1] = -1
	}
	if m.target.O.Heading > m.state.O.Heading {
		m.direction[2] = 1
	} else {
		m.direction[2] = -1
	}
	return &m, nil
}

func (mt *MockTranslator) Level(_ context.Context,
	in *Empty, opts ...grpc.CallOption) (Translator_MoveClient, error) {
	m := MockMover{}
	m.state = proto.Clone(mt.state).(*State)
	m.target = &State{}
	m.target.O = &Orientation{}
	m.target.R = &Rotation{}
	m.t = time.Now()
	m.interval = 500 * time.Millisecond
	m.ctl = proto.Clone(mt.ctl).(*Control)
	if m.target.O.Roll > m.state.O.Roll {
		m.direction[0] = 1
	} else {
		m.direction[0] = -1
	}
	if m.target.O.Pitch > m.state.O.Pitch {
		m.direction[1] = 1
	} else {
		m.direction[1] = -1
	}
	if m.target.O.Heading > m.state.O.Heading {
		m.direction[2] = 1
	} else {
		m.direction[2] = -1
	}
	return &m, nil
}
